﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this.gameObject);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //シーン切り替え
    public static void LoadScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }
}
